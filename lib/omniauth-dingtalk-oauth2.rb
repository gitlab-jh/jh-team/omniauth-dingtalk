# frozen_string_literal: true

require 'omniauth-oauth2'
require 'omniauth-dingtalk/version'
require 'omniauth/strategies/dingtalk'
