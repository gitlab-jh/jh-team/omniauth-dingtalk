# frozen_string_literal: true

RSpec.describe OmniAuth::Dingtalk do
  it "has a version number" do
    expect(OmniAuth::Dingtalk::VERSION).not_to be nil
  end
end
